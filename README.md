# Description
CV.

# Infos
* PHP: 8.1
* Environnement docker


# Installation :
  * récuéperer les sources du projet :
    * git clone https://:{TOKEN}@gitlab.com/some-projects-php/cv.git
  * se connecter au container : <b> docker exec -it cv_php-fpm bash </p> et exécuter :
    * naviguer vers le dossier : /project
      * npm install
  * commande poour compiler le css :
    * node-sass style.scss style.css
