/**
 * Resume/CV template
 */
const resume = {
    firstName: 'Riad',
    lastName: 'SADOK',
    jobTitle: 'Full-stack Developer',
    city: 'Lyon',
    postalCode: '69008',
    country: 'France',
    phone: '+33 638 59 66 xx',
    email: 'riad.sadok@cvmoh.tech',
    education: [
        {
            school: 'UFR De Pau Et Des Pays De L’Adour - France',
            degree: 'Master 2',
            graduationDate: '2018',
            description: 'Systèmes Informatiques pour le Génie de la Logistique Industrielle et des Services'
        },
        {
            school: 'Ecole d\'Informatique d\'Oran - Algérie',
            degree: 'BAC+5',
            graduationDate: '2015',
            description: 'Ingénieur en Systèmes d\'Information'
        }
    ],
    links: [
        {
            label: 'GitLab',
            link: 'https://gitlab.com/riad.sadok31'
        },
        {
            label: 'LinkedIn',
            link: 'https://www.linkedin.com/in/riad-sadok'
        },
        {
            label: 'Website',
            link: 'https://restau-reservation.online'
        },
    ],
    skills: [
        'Php',
        'Drupal',
        'Docker',
        'Symfony',
        'Git',
        'Javascript',
        'Jenkins',
        'NodeJS',
        'Bootstrap',
        'Sass',
        'MySQL',
        'Doctrine',
        'Sonar',
        'Twig',
        'Linux',
        'PhpStorm',
        'Code Review',
        'Swagger',
        'Agile',
    ],
    languages: ['Français', 'English'],
    professionalSummary: `Passionné de code et de nouveaux challenges, je suis spécialisé dans le développement web et en particulier PHP avec le Framework Symfony ou CMS Drupal. J’interviens dans la création, la maintenance, le déploiement, la mise à jour, la migration de vos sites web, ou encore l'intégration des API.`,
    employmentHistory: [
        {
            jobTitle: 'Ingénieur d’études Docker',
            startDate: 'Juin 2023',
            endDate: 'Aou 2023',
            employer: 'Orange Business Services',
            city: 'Nice-Sophia-Antipolis',
            business: 'Orange Business Services | CDI | OCT 2018 - Jusqu\'à présent',
            achievements: [
                'J’ai intégré le projet Coop-IT au sein d’OBS. J’ai travaillé sur l’optimisation des applications métiers d’Orange (Basket, Sola, … ). En tant qu’ingénieur d’étude j’ai pu : ',
                'Mettre en place l’architecture Docker en place.',
                'Réaliser une formation auprès des développeurs sur l’architecture Docker.',
                'Rédiger de documentation technique (architecture, installation, exploitation).'
            ]
        },
        {
            jobTitle: 'Ingénieur d’études PHP et JavaScript',
            startDate: 'Juin 2023',
            endDate: 'Jui 2023',
            employer: 'DOMPLUS Group',
            city: 'Lyon',
            business: '',
            achievements: [
                'Pour aider le client à améliorer la satisfaction de ses usagers, je l’ai accompagné dans la revue de ses outils de relation client (ERP). Mon travail consistait à :',
                'Accompagner et conseiller le client en architecture logicielle (Symphony, ….).',
                'Réaliser des diagrammes de séquences.',
                'Suite à ce travail, le client a engagé Orange Business pour la refonte de son SI.'
            ]
        },
        {
            jobTitle: 'Ingénieur d’études et production Drupal/Wordpress',
            startDate: 'Dec 2022',
            endDate: 'Mai 2023',
            employer: 'Pays de la Loire',
            city: 'Nantes',
            business: '',
            achievements: [
                'Suite à une crise avec le client Région des Pays de la Loire, j’ai été missionné pour auditer et corriger le code du site : www.paysdelaloire.fr/ ',
                'Analyser une anomalie. Ecart de comptes entre la production et la préproduction.',
                'Déployer le code Drupal / Wordpress',
                'Chiffrer les US'
            ]
        },
        {
            jobTitle: 'Développeur DRUPAL',
            startDate: 'Aou 2022',
            endDate: 'Nov 2022',
            employer: 'CLC Loisirs',
            city: 'Lille',
            business: '',
            achievements: [
                'En collaboration avec le Lead Dev et le Chef de Projet. J’ai participé au développement du site web vitrine From Scratch pour location et vente des camping-car @ :  www.libertium.fr/',
                'Participer aux phases de développement : création de block, pages, mise en page...',
                'Proposer des solutions.',
                'Assurer les Best Practices du développement.'
            ]
        },
        {
            jobTitle: 'DEVELOPPEUR BACK-END',
            startDate: 'Juin 2022',
            endDate: 'Jui 2022',
            employer: 'Orange Business Services',
            city: 'Lyon',
            business: '',
            achievements: [
                'En collaboration avec le Chef de Projet et la Team Delivery API. J’ai migré la librairie PHP de la version 2 vers la version 3 qui comporte des nouvelles fonctionnalités Audio et Image. Afin de la présenter aux futurs clients d’Orange.',
                'Réaliser la revue de code.',
                'Etablir une architecture web.',
                'Migrer Live Identity de la version v2 vers la version v3.'
            ]
        },
        {
            jobTitle: 'DEVELOPPEUR DRUPAL',
            startDate: 'Fév 2022',
            endDate: 'Juin 2022',
            employer: 'OURA',
            city: 'Toulouse',
            business: '',
            achievements: [
                'J’ai réalisé la maintenance du site web de la mobilité en Auvergne-Rhône Alpes. @ :  www.oura.com',
                'Apporter des solutions techniques adaptées (les mineurs ne doivent plus utiliser la carte bancaire de leurs parents sur la plateforme).',
                'Assurer la maintenance évolutive (migration, upgrade, …)'
            ]
        },
        {
            jobTitle: 'DEVELOPPEUR FULLSTACK',
            startDate: 'Nov 2020',
            endDate: 'Fév 2022',
            employer: 'Orange Business Services',
            city: 'Nantes',
            business: '',
            achievements: [
                'En relation avec le Lead et PO, en charge du site Orange Cloud web @ :  www.cloud.orange-business.com/',
                'Test d\'intégration',
                'Mettre en place l\'API Marketo, Api pour diffuser des formulaires',
                'Réaliser le dysfonctionnements et les bonnes pratiques'
            ]
        },
        {
            jobTitle: 'DEVELOPPEUR SYMFONY',
            startDate: 'Oct 2020',
            endDate: 'Oct 2022',
            employer: 'Orange Business Services',
            city: 'Lyon',
            business: '',
            achievements: [
                'Présenter à l\'équipe un POC (Proof of concept)',
                'Réalisation d’un outil collaboratif visuel de partage d’humeur « Mood ». Utilisé jusqu’à présent par les équipes d\'Orange.',
                'Conception et réalisation.',
                'Chiffrer les US'
            ]
        },
        {
            jobTitle: 'DEVELOPPEUR DRUPAL',
            startDate: 'Avr 2019',
            endDate: 'Sep 2020',
            employer: 'Orange Business Services',
            city: 'Lyon',
            business: '',
            achievements: [
                'En relation avec le Lead et Product Owner, en charge du site web @ :  www.orange-business.com/',
                'Mettre en place l\'API OBL (Orange Business Lounge), API pour la récupération des informations tarifaires et accords internationaux dans le cadre des offres mobiles entreprise.',
                'Mettre en place l\'API Marketo, Api pour diffuser des formulaires.',
                'Développer de nouvelles fonctionnalités backoffice side.',
                'Refonte des templates Frontoffice / Twig.',
                'Corriger les bugs / js, boosted.',
                'Tester la qualité de code / Sonar'
            ]
        },
        {
            jobTitle: 'DEVELOPPEUR DRUPAL',
            startDate: 'Oct 2018',
            endDate: 'Jui 2019',
            employer: 'D38',
            city: 'Lyon',
            business: '',
            achievements: [
                'En charge du site Département de l\'Isère qui a pour but d\'optimiser ses relations aux usagers. La plateforme est basée sur la solution Capdemat.',
                'Mettre en place l\'API Contact Every One SMS',
                'Corriger les bugs'
            ]
        },
        {
            jobTitle: 'DEVELOPPEUR SYMFONY JR',
            startDate: 'Mar 2018',
            endDate: 'Sep 2018',
            employer: 'Orange Unité d\'Intervention',
            city: 'Saint-Priest',
            business: 'Orange | Stage',
            achievements: [
                'J’ai réalisé l\'outil FOVE pour Orange Unité d’Intervention. Fove génère un synoptique FTTH afin que les techniciens repèrent tous les points de branchements, les points d’aboutement... sur plusieurs départements. Outil qui existe à ce jour.',
                'Analyser les processus existants et recueillir les besoins utilisateurs.',
                'Concevoir et implémenter une application facilitant la génération d\'une fiche immeuble à destination du syndic de copropriété.',
                'Mettre en place une stratégie de déploiement du nouvel outil vers leurs partenaires.',
                'Maintenir les outils informatiques existant.'
            ]
        },
        {
            jobTitle: 'DEVELOPPEUR SYMFONY JR',
            startDate: 'Avr 2017',
            endDate: 'Sep 2017',
            employer: 'Orange Business Services',
            city: 'Bagnolet',
            business: 'Orange Business Services | Stage',
            achievements: [
                'J’ai réalisé un ITSM (gestion du parc informatique) pour Orange Business Services. L’outil permet de :',
                'Collecter les informations sur les outils du marché d\'ITSM (en particulier Service Now). Le but est de réaliser une comparaison sur les fonctionnalités, l\'architecture, le positionnement marché des différents outils.',
                'Etudier les différentes briques du service management (incidents, changes, problème, inventaire, ... ), et faire le lien avec les notions d\'ITIL.',
                'Rédiger un cahier de charge pour un nouvel outil qui répondra au besoin d\'OBS (Orange Business Service).',
                'Développer un outil d\'ITSM, qui a pris en charge les différentes briques du «service management» : changes, inventaire, version'
            ]
        }

    ],
    photo: 'assets/images/photo-cv.jpg',
}

const formatResume = (r) => ({
    ...r,
    address: [
        r.country,
        r.city,
        r.postalCode
    ].filter(Boolean).join(', ')
})

new Vue({
    el: "#app",
    data: formatResume(resume)
});

/**
 * Wait for animatable-component to be loaded (Only for VanillaJS)
 **/
function animatableLoaded() {
    document.querySelector('body').classList.remove('d-none');
}

if (customElements) {
    customElements.whenDefined('animatable-component').then(animatableLoaded);
} else animatableLoaded()